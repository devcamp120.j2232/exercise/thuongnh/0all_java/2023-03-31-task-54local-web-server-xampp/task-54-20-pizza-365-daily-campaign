package com.devcamp.s50.task5420;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5420Application {

	public static void main(String[] args) {
		SpringApplication.run(Task5420Application.class, args);
	}

}
